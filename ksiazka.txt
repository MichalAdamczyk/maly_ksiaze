﻿Mały książę

LEONOWI WERTH
Przepraszam wszystkie dzieci za poświęcenie tej książki dorosłemu. Mam ważne ku temu powody: ten dorosły jest moim najlepszym przyjacielem na świecie. Drugi powód: ten dorosły potrafi zrozumieć wszystko, nawet książki dla dzieci. Mam też trzeci powód: ten dorosły znajduje się we Francji, gdzie cierpi głód i chłód. I trzeba go pocieszyć. Jeśli te powody nie wystarczą - chętnie poświęcę tę książkę dziecku, jakim był kiedyś ten dorosły. Wszyscy dorośli byli kiedyś dziećmi. Choć niewielu z nich o tym pamięta. Zmieniam więc moją dedykację:
LEONOWI WERTH,
gdy był małym chłopcem

I
Gdy miałem sześć lat, zobaczyłem pewnego razu wspaniały obrazek w książce opisującej puszczę dziewiczą. Książka nazywała się "Historie prawdziwe". Obrazek przedstawiał węża boa, połykającego drapieżne zwierzę. Oto kopia rysunku:
Mały Książę
W książce było napisane: "Węże boa połykają w całości schwytane zwierzęta. Następnie nie mogą się ruszać i śpią przez sześć miesięcy, dopóki zdobycz nie zostanie strawiona".
Po obejrzeniu obrazka wiele myślałem o życiu dżungli. Pod wpływem tych myśli udało mi się przy pomocy kredki stworzyć mój pierwszy rysunek. Rysunek numer 1. Wyglądał on tak:
Mały Książę
Pokazałem moje dzieło dorosłym i spytałem, czy ich przeraża.
- Dlaczego kapelusz miałby przerażać? - odpowiedzieli dorośli.
Mój obrazek nie przedstawiał kapelusza. To był wąż boa, który trawił słonia. Narysowałem następnie przekrój węża, aby dorośli mogli zrozumieć. Im zawsze trzeba tłumaczyć. Mój rysunek numer 2 wyglądał następująco:
Mały Książę
Dorośli poradzili mi, abym porzucił rysowanie węży zamkniętych oraz otwartych i abym się raczej zajął geografią, historią, arytmetyką i gramatyką. W ten sposób. mając lat sześć, porzuciłem wspaniałą karierę malarską. Zraziłem się niepowodzeniem rysunku numer 1 i numer 2. Dorośli nigdy nie potrafią sami zrozumieć. A dzieci bardzo męczy konieczność stałego objaśniania. Musiałem wybrać sobie inny zawód: zostałem pilotem. Latałem po całym świecie i muszę przyznać, że znajomość geografii bardzo mi się przydała. Potrafiłem jednym rzutem oka odróżnić Chiny od Arizony. Ta wiedza oddaje duże usługi, szczególnie wówczas, gdy się błądzi nocą.
Zawód pilota dał mi okazję do licznych spotkań z wieloma poważnymi ludźmi. Wiele czasu spędziłem z dorosłymi. Obserwowałem ich z bliska. Lecz to nie zmieniło mej opinii o nich. Gdy spotykałem dorosłą osobę, która wydawała mi się trochę mądrzejsza, robiłem na niej doświadczenie z moim rysunkiem numer 1, który stale nosiłem przy sobie. Chciałem wiedzieć, czy mam do czynienia z osobą rzeczywiście pojętną. Lecz za każdym razem odpowiadano mi: - To jest kapelusz. - Wobec tego nie rozmawiałem ani o wężach boa, ani o lasach dziewiczych, ani o gwiazdach. Starałem się być na poziomie mego rozmówcy. Rozmawiałem o brydżu, golfie, polityce i krawatach. A dorosły był zadowolony, że poznał tak rozsądnego człowieka.

II
W ten sposób, nie znajdując z nikim wspólnego języka, prowadziłem samotne życie aż do momentu przymusowego lądowania na Saharze. Było to sześć lat temu. Coś się zepsuło w motorze. Ponieważ nie towarzyszył mi ani mechanik, ani pasażerowie, musiałem sam zabrać się do bardzo trudnej naprawy. Była to dla mnie kwestia życia lub śmierci. Miałem zapas wody zaledwie na osiem dni.
Pierwszego wieczoru zasnąłem na piasku, o tysiąc mil od terenów zamieszkałych. Byłem bardziej osamotniony, niż rozbitek na tratwie pośrodku oceanu. Toteż proszę sobie wyobrazić moje zdziwienie, gdy o świcie obudził mnie czyjś głosik. Posłyszałem:
- Proszę cię, narysuj mi baranka.
- Co takiego?
- Narysuj mi baranka.
Mały Książę
Zerwałem się na równe nogi. Przetarłem dobrze oczy. Natężyłem wzrok. I zobaczyłem niezwykłego małego człowieka, który bacznie mi się przyglądał. Oto jego najlepszy portret, który udało mi się zrobić później:
Oczywiście nie jest on na tym rysunku tak czarujący jak w rzeczywistości. To nie moja wina. Gdy miałem sześć lat, dorośli zniechęcili mnie do malarstwa i dlatego umiem rysować tylko węże boa zamknięte i otwarte.
Patrzyłem na to zjawisko oczyma okrągłymi ze zdziwienia. Nie zapominajcie, że znajdowałem się o tysiąc mil od terenów zamieszkałych.
Tymczasem człowieczek nie wyglądał ani na zabłąkanego, ani na ginącego ze zmęczenia, ani na umierającego z pragnienia czy głodu, ani na przestraszonego. W niczym nie przypominał dziecka zgubionego w środku pustyni, o tysiąc mil od miejsc zamieszkałych. Gdy wreszcie odzyskałem mowę, odezwałem się:
- Ale... cóż ty tutaj robisz?
A on powtórzył bardzo powoli, jak gdyby chodziło o niezwykle ważną sprawę:
- Proszę cię, narysuj mi baranka ...
Zawsze ulega się urokowi tajemnicy. Pomimo niedorzeczności sytuacji - byłem bowiem o tysiąc mil od terenów zamieszkałych i grozi mi niebezpieczeństwo śmierci - wyciągnąłem z kieszeni kartkę papieru i wieczne pióro. W tym momencie przypomniałem sobie, że przecież uczyłem się tylko geografii, historii, rachunków i gramatyki, więc zmartwiony powiedziałem chłopcu, że nie umiem rysować. Ale on odrzekł:
- To nic nie szkodzi. Narysuj mi baranka.
Ponieważ nigdy w życiu nie rysowałem baranka, pokazałem mu jeden z dwóch rysunków, jakie umiałem zrobić: rysunek węża boa zamkniętego. Ku memu zdziwieniu chłopczyk odpowiedział:
Mały Książę
- Nie, nie. Nie chcę słonia połkniętego przez węża boa. Boa jest zbyt niebezpieczny, a słoń za duży. Mam za mało miejsca. Potrzebny mi jest baranek. Narysuj mi baranka.
Narysowałem baranka. Mały przyjrzał się uważnie i rzekł:
- Nie, ten baranek jest już bardzo chory. Zrób innego.
Mały Książę
Narysowałem.
Mały przyjaciel uśmiechnął się grzecznie z pobłażaniem:
- Przyjrzyj się. To nie jest baranek, to baran. On ma rogi.
Mały Książę
Zrobiłem nowy rysunek, ale został odrzucony tak jak poprzedni:
- Ten baranek jest za stary. Chcę mieć baranka, który będzie długo żył.
Tracąc już cierpliwość - chciałem bowiem jak najprędzej zabrać się do naprawy motoru - nabazgrałem ten obrazek i powiedziałem:
Mały Książę
- To jest skrzynka. Baranek, którego chciałeś mieć, jest w środku.
Byłem bardzo zdziwiony, widząc radość na buzi małego krytyka.
- To jest właśnie to, czego chciałem. Czy myślisz, ze trzeba dużo trawy dla tego baranka?
- Dlaczego pytasz?
- Bo mam tak mało miejsca ...
- Na pewno wystarczy. Dałem ci zupełnie małego baranka.
Pochylił główkę nad rysunkiem.
- Nie taki znowu mały. Zobacz, zasnął ...
Tak wyglądał początek mojej znajomości z Małym Księciem.

III
Dużo czasu upłynęło, zanim zrozumiałem, skąd przybył. Mały Książę, choć sam zadawał mi wiele pytań udawał, że moich nie słyszy dopiero z wypowiedzianych przypadkowo słów poznałem jego historię. Kiedy po raz pierwszy ujrzał mój samolot (nie narysuję mego samolotu, ponieważ jest to rysunek zbyt trudny), zapytał:
Mały Książę
- Co to za przedmiot?
- To nie jest przedmiot. To lata. To samolot. To mój samolot.
Byłem bardzo dumny, mogąc mu powiedzieć, latam. Wtedy zapytał:
- Jak to? Spadłeś z nieba?
- Tak - odparłem skromnie.
- Ach, to zabawne...
I Mały Książę wybuchnął śmiechem, który mnie rozgniewał. Wolałbym, aby poważniej traktowano moje nieszczęście. Po chwili powiedział:
- A więc ty też spadłeś z nieba? Z jakiej planety pochodzisz?
W tym momencie rozjaśnił mi się nieco mrok otaczający jego zjawienie się i spytałem natychmiast:
- Więc ty przybyłeś z innej planety?
Ale on nie odpowiadał. Schylił głowę i uważnie przyglądał się memu samolotowi.
- To prawda. Przy pomocy czegoś takiego nie mogłeś przybyć z daleka.
I pogrążył się w rozmyślaniach. Następnie wyjął z kieszeni baranka i zaczął uważnie przyglądać się swemu skarbowi.
Możecie sobie wyobrazić, jak bardzo byłem zaintrygowany tym zagadkowym zwierzeniem o innych planetach. Starałem się zatem dowiedzieć czegoś więcej.
- Skąd przybyłeś, mój mały? Gdzie jest twój dom? Dokąd chcesz zabrać baranka?
Po chwili pełnej skupienia powiedział:
- Dobrą stroną tej skrzynki, którą mi dałeś, jest to, że będzie w nocy jego domkiem.
- Naturalnie. Jeżeli będziesz grzeczny, dam ci jeszcze linkę i palik, abyś mógł go w dzień przywiązywać.
Ta propozycja uraziła Małego Księcia.
- Przywiązywać? Też pomysł!
- Jeżeli go nie przywiążesz, to pójdzie gdziekolwiek i zginie.
Mój mały przyjaciel znowu się roześmiał.
- A gdzież on ma iść?
- Gdziekolwiek, prosto przed siebie.
Wtedy Mały Książę powiedział z powagą:
- To nie ma znaczenia. Ja mam tak mało miejsca.
Następnie dorzucił z odrobiną - jak mi się wydawało - smutku:
- Idąc prosto przed siebie nie można zajść daleko ...

IV
W ten sposób dowiedziałem się drugiej ważnej rzeczy: że planeta, z której pochodził, niewiele była większa od zwykłego domu. Ta wiadomość nie zdziwiła mnie. Wiedziałem dobrze, że oprócz dużych planet, takich jak Ziemia, Jowisz, Mars, Wenus, którym nadano imiona, są setki innych, tak małych, że z wielkim trudem można je zobaczyć przy pomocy teleskopu. Kiedy astronom odkrywa którąś z nich, daje jej zamiast imienia numer. Nazywa ją na przykład gwiazdą 3251.
Mały Książę
Miałem pewne podstawy, aby sądzić, że planeta, z której przybył Mały Książę, jest gwiazdą B-612. Ta gwiazda była widziana raz tylko, w 1909 roku, przez tureckiego astronoma, który swoje odkrycie ogłosił na Międzynarodowym Kongresie Astronomów. Nikt jednak nie chciał mu uwierzyć, ponieważ miał bardzo dziwne ubranie. Tacy bowiem są dorośli ludzie.
Mały Książę
Na szczęście dla planety B-612 turecki dyktator kazał pod karą śmierci zmienić swojemu ludowi ubiór na europejski. Astronom ogłosił po raz wtóry swoje odkrycie w roku 1920 - i tym razem był ubrany w elegancki frak. Cały świat mu uwierzył.
Opowiedziałem wam te szczegóły o planecie B-612 i podałem numer ze względu na dorosłych. Dorośli są zakochani w cyfrach. Jeżeli opowiadacie im o nowym przyjacielu, nigdy nie spytają o rzeczy najważniejsze. Nigdy nie usłyszycie: "Jaki jest dźwięk jego głosu? W co lubi się bawić? Czy zbiera motyle?"
Oni spytają was: "Ile ma lat? Ilu ma braci? Ile waży? Ile zarabia jego ojciec?" Wówczas dopiero sądzą, że coś wiedzą o waszym przyjacielu.
Jeżeli mówicie dorosłym: "Widziałem piękny dom z czerwonej cegły, z geranium w oknach i gołębiami na dachu" - nie potrafią sobie wyobrazić tego domu. Trzeba im powiedzieć: "Widziałem dom za sto tysięcy złotych". Wtedy krzykną: "Jaki to piękny dom!"
Jeżeli powiecie dorosłym: "Dowodem istnienia Małego Księcia jest to, że był śliczny, że śmiał się i że chciał mieć baranka, a jeżeli chce się mieć baranka, to dowód, że się istnieje" - wówczas wzruszą ramionami i potraktują was jak dzieci. Lecz jeżeli im powiecie, że przybył z planety B-612, uwierzą i nie będą zadawać niemądrych pytań. Oni są właśnie tacy. Nie można od nich za dużo wymagać. Dzieci muszą być bardzo pobłażliwe w stosunku do dorosłych.
Mały Książę
My jednak, którzy dobrze rozumiemy życie, kpimy sobie z cyfr. Chciałbym zacząć tę historię, jak zaczyna się baśń. Wolałbym powiedzieć: "Był pewnego razu Mały Książę, który mieszkał na planecie troszeczkę większej od niego i który bardzo chciał mieć przyjaciela ..."
Dla tych, którzy znają życie, wyglądałoby to o wiele prawdziwiej.
Byłoby mi przykro, gdyby moją książkę traktowano niepoważnie. Z wielkim bólem opowiadam te wspomnienia. Sześć lat minęło już od chwili, kiedy mój przyjaciel odszedł ze swoim barankiem. Próbuję opisać go po to, aby nie zapomnieć. To bardzo przykre zapomnieć przyjaciela. A przecież nie każdy dorosły ma przyjaciela. I mógłbym stać się podobny do dorosłych, którzy interesują się tylko cyframi. Dlatego też kupiłem sobie pudełko z farbami i ołówki. Bardzo trudno zabrać się do rysowania w moim wieku, tym bardziej, że jedyne próby w tym kierunku, to były rysunki węża boa zamkniętego i otwartego, rysunki, które robiłem mając sześć lat. Postaram się jednak, aby portrety były jak najwierniejsze. Sam nie jestem pewien, czy mi się to uda. Jeden rysunek jest dobry, drugi gorszy. Myli mi się trochę wzrost Małego Księcia. Tutaj jest za duży, tam znów za mały. Waham się, malując kolory jego stroju. Błądzę w ciemnościach wspomnień i w rezultacie mylę się w rzeczach bardzo zasadniczych. Ale trzeba mi to wybaczyć. Mój przyjaciel nigdy mi nic nie objaśniał. Uważał pewnie, że jestem podobny do niego. Ja jednak nie potrafię, niestety, widzieć baranka przez ściany skrzynki. Możliwe więc, że jestem trochę podobny do dorosłych. Juz się prawdopodobnie zestarzałem.


V
Codziennie dowiadywałem się czegoś nowego o planecie, o wyjeździe, o podróży. Wiadomości te gromadziły się z wolna i przypadkowo. I tak trzeciego dnia poznałem dramat baobabów.
W tym przypadku stało się to dzięki barankowi. Mały Książę spytał mnie nagle, jakby w coś zwątpił:
- Czy to prawda, że baranki zjadają krzaki?
- Tak, to prawda.
- O, to bardzo się z tego cieszę.
- Nie rozumiałem, jakie znaczenie może mieć wiadomość, że baranki zjadają krzaki. Ale Mały Książę dodał:
- Wobec tego one jedzą także baobaby?
Mały Książę
Wytłumaczyłem mu, że baobaby nie są krzakami, lecz drzewami, i to tak dużymi jak kościoły, i gdyby nawet zabrał ze sobą całe stado słoni, to nie dałyby one rady jednemu baobabowi.
Na myśl o stadzie słoni Mały Książę roześmiał się:
- Trzeba byłoby poustawiać jednego na drugim.
Później dodał z namysłem:
- Ale nim baobaby staną się drzewami, są malutkie.
- To prawda. Ale dlaczego chcesz, żeby baranki zjadały małe baobaby?
Mały Książę
Odpowiedział mi: - Dobrze, dobrze... - jakby chodziło o rzecz najzupełniej oczywistą. Musiałem zrobić duży wysiłek, aby zrozumieć to bez niczyjej pomocy. Okazało się, że na planecie Małego Księcia, tak jak na wszystkich planetach, rosły rośliny pożyteczne oraz zielska. W rezultacie znajdowały się tam dobre nasiona roślin pożytecznych i złe nasiona zielsk. Ale ziarna są niewidoczne. Śpią sobie skrycie w ziemi aż do chwili, kiedy któremuś z nich przyjdzie ochota obudzić się. Wypuszcza wtedy cudowny, bezbronny pęd, który najpierw nieśmiało wyciąga się ku słońcu. Jeżeli jest to pęd rzodkiewki albo róży, można mu pozwolić rosnąć, jak chce. Ale jeżeli jest to zielsko,trzeba wyrwać je jak najszybciej, gdy tylko się je rozpozna. Otóż na planecie Małego Księcia były ziarna straszliwe. Ziarna baobabu. Zakażony był nimi cały grunt. A kiedy baobab wyrośnie, to na wyrwanie jest za późno i nigdy już nie można się go pozbyć. Zajmie całą planetę. Przeorze ją korzeniami. A jeżeli planeta jest mała, a baobabów jest dużo, to one ją rozsadzają.
- Jest to kwestia dyscypliny - powiedział mi później Mały Książę. - Rano, po umyciu się, trzeba robić bardzo dokładną toaletę planety. Trzeba się zmusić do regularnego wyrywania baobabów, i to natychmiast po odróżnieniu ich od krzewów róży, do których są w młodości bardzo podobne. Jest to praca bardzo nudna, lecz bardzo łatwa ...
Pewnego dnia poradził mi, abym spróbował narysować ładny obrazek, który by pomógł dzieciom z naszej planety zrozumieć niebezpieczeństwo baobabów.
Mały Książę
-Gdyby kiedyś podróżowały - mówił mi - może im się przydać. Czasem odłożenie pracy na później nie przynosi szkody. Lecz w wypadku baobabu jest to zawsze katastrofą. Znałem planetę, którą zamieszkiwał leniuch. Zlekceważył trzy pędy ...
Według wskazówek Małego Księcia narysowałem tę planetę. Nie lubię prawić morałów. Lecz niebezpieczeństwo baobabów jest tak mało znane, a ryzyko, na jakie narażają się ci, którzy zabłądzą na jedną z małych planet, jest tak poważne, że tym razem odstąpię od mych zasad. I mówię: dzieci, uważajcie na baobaby!
Wiele pracy kosztował mnie ten rysunek, który zrobiłem po to, aby ostrzec mych przyjaciół przed niebezpieczeństwem od dawna grożącym, a nieznanym. Mój trud jednakże opłacił się. Może zapytacie, dlaczego w tej książce nie ma innych równie wspaniałych rysunków jak rysunek baobabów? Odpowiedź jest prosta: próbowałem je zrobić, lecz mi się nie udały. Gdy malowałem baobaby, kierowała mną jakaś wewnętrzna konieczność.

VI
Powoli zrozumiałem, Mały Książę, twoje smutne życie. Od dawna jedyną rozrywką był dla ciebie urok zachodów słońca. Ten nowy szczegół twego życia poznałem czwartego dnia rano, gdyś mi powiedział:
- Bardzo lubię zachody słońca. Chodźmy zobaczyć zachód słońca.
- Ale trzeba poczekać.
Mały Książę
- Poczekać na co?
- Poczekać, aż słońce zacznie zachodzić.
Początkowo zrobiłeś zdziwioną minę, a później roześmiałeś się i rzekłeś:
- Ciągle mi się wydaje, że jestem u siebie.
Rzeczywiście, wszyscy wiedzą, że kiedy w Stanach Zjednoczonych jest godzina dwunasta, we Francji słońce zachodzi. Gdyby można się było w ciągu minuty przenieść ze Stanów do Francji,oglądałoby się zachód słońca. Niestety, Francja jest daleko. Ale na twojej planecie mogłeś przesunąć krzesełko o parę kroków i oglądać zachód słońca tyle razy, ile chciałeś ...
- Pewnego dnia oglądałem zachód słońca czterdzieści trzy razy - powiedział Mały Książę, a w chwilę później dodał:
- Wiesz, gdy jest bardzo smutno, to kocha się zachody słońca.
- Więc wówczas gdy oglądałeś je czterdzieści trzy razy, byłeś aż tak bardzo smutny? - zapytałem.
Ale Mały Książę nie odpowiedział.

VIII
Wkrótce poznałem lepiej ten kwiat. Na planecie Małego Księcia kwiaty były zawsze bardzo skromne, o pojedynczej koronie płatków, nie zajmujące miejsca i nie przeszkadzające nikomu. Pojawiały się któregoś ranka wśród traw i więdły wieczorem. Krzak róży wykiełkował w ciągu dnia z ziarna przyniesionego nie wiadomo skąd i Mały Książę z uwagą śledził ten pęd, zupełnie niepodobny do innych pędów.Mógł to być nowy gatunek baobabu. Lecz krzak szybko przestał rosnąć i zaczął się formować kwiat. Mały Książę, który śledził pojawienie olbrzymiego pąka, wyczuwał, iż wykwitnie z niego jakieś cudowne zjawisko, lecz róża schowana w swoim zielonym domku przygotowywała się powoli. Starannie dobierała barw. Ubierała się wolno, dopasowywała płatki jeden do drugiego. Nie chciała rozkwitnąć pognieciona jak maki. Pragnęła zjawić się w pełnym blasku swojej piękności. O, tak! Była wielką zalotnicą. Jej tajemnicze strojenie trwało wiele dni. Aż pewnego poranka - dokładnie o wschodzie słońca - ukazała się.
I oto ona - która tyle trudu włożyła w swój staranny wygląd- powiedziała ziewając:
- Ach, dopiero się obudziłam... Przepraszam bardzo... Jestem jeszcze nie uczesana.
Mały Książę
Mały Książę nie mógł powstrzymać słów z zachwytu:
- Jakaż pani jest piękna!
- Prawda odpowiedziała róża cichutko.
- Urodziłam się równocześnie ze słońcem.
Mały Książę domyślił się, że róża nie jest zbyt skromna, lecz jakżeż była wzruszająca!
- Sądzę, że czas na śniadanie - dorzuciła po chwili - czy byłby pan łaskaw pomyśleć o mnie?
Mały Książę
Mały Książę, bardzo zawstydzony, poszedł po konewkę i podał jej świeżą wodę.
Wkrótce swą trochę płochliwą próżnością zaczęła go torturować. Pewnego dnia na przykład, mówiąc o swych czterech kolcach, powiedziała:
- Mogą zjawić się tygrysy uzbrojone w pazury...
- Nie ma tygrysów na mojej planecie - sprzeciwił się Mały Książę - a poza tym tygrysy nie jedzą trawy.
- Nie jestem trawą - odparła słodko róża.
- Proszę mi wybaczyć...
Mały Książę
- Nie obawiam się tygrysów, natomiast czuję wstręt do przeciągów. Czy nie ma pan parawanu?
"Wstręt do przeciągów to nie jest dobre dla rośliny - pomyślał Mały Książę. - Ten kwiat jest bardzo skomplikowany."
- Wieczorem proszę mnie przykryć kloszem. U pana jest bardzo zimno. Złe są tu urządzenia. Tam, skąd przybyłam...
Urwała. Przybyła w postaci nasienia. Nie mogła znać innych planet. Naiwne kłamstwo, na którym dała się przyłapać, zawstydziło ją. Zakaszlała dwa lub trzy razy, aby pokryć zażenowanie.
- A ten parawan?
- Ja bym przyniósł, ale pani mówiła...
Mały Książę
Wtedy róża znów zaczęła kaszleć, aby Mały Książę miał wyrzuty sumienia. W ten sposób mimo dobrej woli płynącej z jego uczucia Mały Książę przestał wierzyć róży. Wziął poważnie słowa bez znaczenia i stał się bardzo nieszczęśliwy.
- Nie powinienem jej słuchać - zwierzył mi się któregoś dnia - nigdy nie trzeba słuchać kwiatów. Trzeba je oglądać i wąchać. Mój kwiat napełniał całą planetę swoją wonią, lecz nie umiałem się nim cieszyć. Historia kolców, która tak mnie rozdrażniła, powinna rozczulić...
Zwierzył się jeszcze:
- Nie potrafiłem jej zrozumieć. Powinienem sądzić ją według czynów, a nie słów. Czarowała mnie pięknem i zapachem. Nie powinienem nigdy od niej uciec. Powinienem odnaleźć w niej czułość pod pokrywką małych przebiegłostek. Kwiaty mają w sobie tyle sprzeczności. Lecz byłem za młody, aby umieć ją kochać.

IXrozdzial 9
bfdfdbd
bfd


db
